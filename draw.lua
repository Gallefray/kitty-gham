function drawMap()
    for i = 1, game.mapW do
    	for j = 1, game.mapH do
	    love.graphics.setColor(255, 255, 255, 255)
	    	if game.map[j][i] == -1 then
	    		love.graphics.rectangle("line", i*game.ts+xmod,
				       j*game.ts+ymod)
      	    elseif game.map[j][i] == air[1] then
		love.graphics.draw(quad.i_air, quad.air[1], i*game.ts+xmod,
				       j*game.ts+ymod)
	    elseif game.map[j][i] == air[2] then
		love.graphics.draw(quad.i_air, quad.air[2], i*game.ts+xmod,
				       j*game.ts+ymod)
    	    elseif game.map[j][i] == air[3] then
		love.graphics.draw(quad.i_air, quad.air[3], i*game.ts+xmod,
				       j*game.ts+ymod)
	    elseif game.map[j][i] == air[4] then
		love.graphics.draw(quad.i_air, quad.air[4], i*game.ts+xmod,
				       j*game.ts+ymod)
	    elseif game.map[j][i] == air[5] then
		love.graphics.draw(quad.i_air, quad.air[5], i*game.ts+xmod,
				       j*game.ts+ymod)
	    elseif game.map[j][i] == air[6] then
		love.graphics.draw(quad.i_air, quad.air[6], i*game.ts+xmod,
				       j*game.ts+ymod)

		
      	    elseif game.map[j][i] == wall[1] or game.map[j][i] == invislava[1] or game.map[j][i] == fakewall[1] then
		love.graphics.draw(quad.i_wall, quad.wall[1], i*game.ts+xmod,
				       j*game.ts+ymod)
	    elseif game.map[j][i] == wall[2] or game.map[j][i] == invislava[2] or game.map[j][i] == fakewall[2] then
		love.graphics.draw(quad.i_wall, quad.wall[2], i*game.ts+xmod,
				       j*game.ts+ymod)
    	    elseif game.map[j][i] == wall[3] or game.map[j][i] == invislava[3] or game.map[j][i] == fakewall[3] then
		love.graphics.draw(quad.i_wall, quad.wall[3], i*game.ts+xmod,
				       j*game.ts+ymod)
	    elseif game.map[j][i] == wall[4] or game.map[j][i] == invislava[4] or game.map[j][i] == fakewall[4] then
		love.graphics.draw(quad.i_wall, quad.wall[4], i*game.ts+xmod,
				       j*game.ts+ymod)
		 
    	    elseif game.map[j][i] == door then
    		love.graphics.setColor(0, 0, 0, 255)
    		love.graphics.rectangle("fill", i*game.ts+xmod, 
    					j*game.ts+ymod, game.ts, game.ts)

    	    elseif game.map[j][i] == lava[1] or game.map[j][i] == fakelava[1] then
    		love.graphics.draw(quad.i_air, quad.air[1], i*game.ts+xmod,
				       j*game.ts+ymod)
    		anim.lava:draw(i*game.ts+xmod, j*game.ts+ymod) 
    		elseif game.map[j][i] == lava[2] or game.map[j][i] == fakelava[2] then
    		love.graphics.draw(quad.i_air, quad.air[2], i*game.ts+xmod,
				       j*game.ts+ymod)
    		anim.lava:draw(i*game.ts+xmod, j*game.ts+ymod) 
    		elseif game.map[j][i] == lava[3] or game.map[j][i] == fakelava[3] then
    		love.graphics.draw(quad.i_air, quad.air[3], i*game.ts+xmod,
				       j*game.ts+ymod)
    		anim.lava:draw(i*game.ts+xmod, j*game.ts+ymod) 
    		elseif game.map[j][i] == lava[4] or game.map[j][i] == fakelava[4] then
    		love.graphics.draw(quad.i_air, quad.air[4], i*game.ts+xmod,
				       j*game.ts+ymod)
    		anim.lava:draw(i*game.ts+xmod, j*game.ts+ymod) 
    		--love.graphics.rectangle("line", i*game.ts+xmod, 
    		--		j*game.ts+ymod, game.ts, game.ts)	
			elseif game.map[j][i] == fakewall[1] or game.map[j][i] == inviswall[1] then
			love.graphics.draw(quad.i_air, quad.air[1], i*game.ts+xmod,
				       j*game.ts+ymod)
	    	elseif game.map[j][i] == fakewall[2] or game.map[j][i] == inviswall[2] then
			love.graphics.draw(quad.i_air, quad.air[2], i*game.ts+xmod,
				       j*game.ts+ymod)
    	    elseif game.map[j][i] == fakewall[3] or game.map[j][i] == inviswall[3] then
			love.graphics.draw(quad.i_air, quad.air[3], i*game.ts+xmod,
				       j*game.ts+ymod)
	    	elseif game.map[j][i] == fakewall[4] or game.map[j][i] == inviswall[4] then
			love.graphics.draw(quad.i_air, quad.air[4], i*game.ts+xmod,
				       j*game.ts+ymod)
    	    end
    	end
    end
end


