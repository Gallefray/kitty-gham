function edit_load()
    editor = {}
    mouse = {}

    editor.map = {}
    editor.mapw = 25 -- 25 tiles width
    editor.maph = 13 -- 13 tiles height
    editor.mapname = ""
    editor.mapnum = 0

    local tab = {}
    for j = 0, editor.maph do
    	for i = 0, editor.mapw do
    		table.insert(tab, -1) 
    	end
    	table.insert(editor.map, tab)
    end

    mouse.x = (love.mouse.getX())-32
    mouse.y = (love.mouse.getY())-32
    mouse.tx = (mouse.x - (mouse.x % game.ts))
    mouse.ty = (mouse.y - (mouse.y % game.ts))
    -- mouse.tx = mouse.x/game.ts
    -- mouse.ty = mouse.y/game.ts
end

function edit_draw()
    drawMap()
    -- love.graphics.setColor(34, 112, 166)
    -- love.graphics.rectangle("fill", mouse.tx, mouse.ty, 32, 32)
    love.graphics.setColor(255, 0, 0, 100)
    love.graphics.rectangle("line", mouse.tx, mouse.ty, game.ts, game.ts)
end

function edit_update()
    mouse.x = love.mouse.getX() + xmod
    mouse.y = love.mouse.getY() + ymod
    mouse.tx = mouse.x/game.ts
    mouse.ty = mouse.y/game.ts
end
