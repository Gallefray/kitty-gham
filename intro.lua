function intro_load()
	intro = {
		font = love.graphics.newImageFont("data/font/intro_font.png",
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890" ..
		"abcdefghijknmnopqrstuvwxyz!.?@#$%£%()"),
		-- > cont slowly fades in/out < --
		cont = "(Press space/enter to continue)",
		text = {
			{text="There is no goal in this game. Except opening doors.",
			 r=255,g=255,b=255,a=0,
			 x=30, y=30
			},
			{text="No story, highscore or music.",
			 r=255,g=255,b=255,a=0,
			 x=80, y=80
			},
			{text="There are no enemies except your own fear and perception.",
			 r=255,g=255,b=255,a=0,
			 x=130, y=130
			},
			{text="Just have fun.",
			 r=255,g=255,b=255,a=0,
			 x=180, y=180
			},
			{text="Game by Finn O'leary (@Gallefray)",
			 r=255,g=255,b=255,a=0,
			 x=230, y=230
			},
			{text="Art by bitslap (@slapmybitsup / bitslap.se)",
			 r=255,g=255,b=255,a=0,
			 x=280, y=280
			},
		},
		textsel = 1
	}
	-- love.graphics.setFont(intro.font)
end

function intro_draw()
	local textsel = intro.textsel
	love.graphics.setColor(intro.text[textsel].r, intro.text[textsel].g,
						   intro.text[textsel].b, intro.text[textsel].a)
	love.graphics.print(intro.cont, 30, 512)
	love.graphics.print(intro.text[textsel].text, intro.text[textsel].x,
						intro.text[textsel].y)
end

function intro_update(dt)
	intro.text[intro.textsel].a = tween_var(intro.text[intro.textsel].a, 255, 1)
end

function tween_var(var, targ, spd)
	if (var + spd) < targ then
		return var + spd
	elseif (var + spd) > targ then
		return var - spd
	else
		return var
	end
end
