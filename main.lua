require 'intro'
require 'levels'
lurker = require "lib/lurker"
require "lib/AnAL"

function love.load()
	require 'editor'
    load_img()
    load()
    edit_load()
	intro_load()
end

function load_img()    
    anim = { player = {}}
    quad = { air = {}, wall = {}}
    local a_l = love.graphics.newImage("data/img/lavatile.png")
    anim.lava = newAnimation(a_l, 32, 32, 0.1, 0)
    local a_ps = love.graphics.newImage("data/img/kitty-stand.png")
    anim.player.stand = newAnimation(a_ps, 32, 32, 0.1, 4)
    local a_pw = love.graphics.newImage("data/img/kitty-walk.png")
    anim.player.walk = newAnimation(a_pw, 32, 32, 0.1, 4)

    quad.i_air = love.graphics.newImage("data/img/air.png")
    quad.i_wall = love.graphics.newImage("data/img/wall.png")

    quad.air[1] = love.graphics.newQuad(0, 0, 32, 32, 64, 96)
    quad.air[2] = love.graphics.newQuad(32, 0, 32, 32, 64, 96)
    quad.air[3] = love.graphics.newQuad(0, 32, 32, 32, 64, 96)
    quad.air[4] = love.graphics.newQuad(32, 32, 32, 32, 64, 96)
    -- grass bottom:
    quad.air[5] = love.graphics.newQuad(0, 64, 32, 32, 64, 96)
    -- wall bottom:
    quad.air[6] = love.graphics.newQuad(32, 64, 32, 32, 64, 96)
    
    quad.wall[1] = love.graphics.newQuad(0, 0, 32, 32, 128, 32)
    quad.wall[2] = love.graphics.newQuad(32, 0, 32, 32, 128, 32)
    quad.wall[3] = love.graphics.newQuad(64, 0, 32, 32, 128, 32)
    quad.wall[4] = love.graphics.newQuad(96, 0, 32, 32, 128, 32)

    require "draw"
end

function load()
	love.window.setTitle("Press 'space' to grow up")

    screen = {}
    game = {}
    world = {} 
    tween = {l = {}}
    player = {}

    screen.tx = 2
    screen.ty = 92
    xmod = screen.tx-32
	ymod = screen.ty-32

    game.mode = "intro"
    game.mapn = INTRO
    game.mapn = game.mapn + 1
    game.map = intro
    game.mapW = 25 -- 25
    game.mapH = 13 -- 13
    game.ts = 32

    world.gy = 0.4
    world.aresx = 0.5
    world.aresy = 0.5

    -- Player
    -- Be.
    player.x = 64.0
    player.y = 32.0
    player.vy = 0.1
    player.h = game.ts
    player.w = game.ts
    -- Move.
    player.dir = "right"
    player.dx = 0.0
    player.dy = 0.0
    player._mSpd = 4.0--20.0
    player._jSpd = 10.0--8.0

    player.hJmp = false

    player.jCnt = 0.0
    player.jStep = 2.0
    player._mjCnt = 64.0--15.0

    -- Tween:
    -- Start: 	243, 132, 0?
    -- End: 	255, 63,  0?
    tween.l.r = 255

    tween.l.min_g = 48--63
    tween.l.max_g = 99--132
    tween.l.g = tween.l.min_g	

    tween.l.b = 0
    tween.l.stp_g = 1
    tween.l.stpd_g = false
end

function love.draw()
	if game.mode == "play" then
    	-- love.graphics.translate(screen.tx, screen.ty)
    	drawMap()
    	drawPlayer()
    elseif game.mode == "edit" then
        edit_draw()
	elseif game.mode == "intro" then
		intro_draw()
    end
end

function love.update(dt)
    -- player.y = 64
    -- player.mapn = HAVE_FAITH_DEAR
    -- player.map = have_faith_dear
    -- level_ld(player.mapn)
    lurker.update()
	if game.mode == "intro" then
		intro_update(dt)
	else
		anim.lava:update(dt)
		anim.player.stand:update(dt)
		anim.player.walk:update(dt)
		if love.keyboard.isDown("escape") then
			love.event.quit()
		end
		if love.keyboard.isDown("e") then
			if game.mode == "play" then
				game.mode = "edit"
			elseif game.mode == "edit" then
				game.mode = "play"
			end	
		end
		if game.mode == "play" then
			playerMov(dt)
			collision(dt)
			tween_f()
		elseif game.mode == "edit" then
			edit_update()
		end
	end
end

function love.keypressed(key, rep)
	if not rep then
		if game.mode == "intro" then
			if key == "return" or key == " " then
				if (intro.textsel < table.maxn(intro.text)) then
					intro.textsel = intro.textsel + 1
				else
					game.mode = "play"
				end
			end
		end
	end
end

function drawPlayer()
    love.graphics.setColor(255, 255, 255, 255)
    if player.dx < 1 and player.dx > -1 then
    	if player.dir == "left" then
    		anim.player.stand:draw(player.x+64+xmod, player.y+32+ymod, 0, -1, 1)
    	elseif player.dir == "right" then
    		anim.player.stand:draw(player.x+32+xmod, player.y+32+ymod)
    	end
    else
    	if player.dir == "left" then
    		anim.player.walk:draw(player.x+64+xmod, player.y+32+ymod, 0, -1, 1)
    	elseif player.dir == "right" then
    		anim.player.walk:draw(player.x+32+xmod, player.y+32+ymod)
    	end
    end
    -- love.graphics.setColor(250, 0, 0, 255)
    -- love.graphics.rectangle("fill", player.x, player.y, player.w, player.h)
    -- love.graphics.setColor(200, 255, 0, 255)
    -- love.graphics.line(player.x, player.y+player.h/2, player.x+player.w, player.y+player.h/2)
end

function playerMov(dt)
    canJump = true
    player.dx = 0
    player.dy = player.vy

    if (chk_collwall() or collides(inviswall)) then
    	-- jumping is only illegal when
    	-- banging your head, not
    	-- if you just fell down
    	canJump = player.dy > 0
    	
    	player.dy = 0
    	player.vy = 0
    else
    	player.vy = player.vy +world.gy
    	-- to avoid clipping through floor tiles
    	if (player.vy > game.ts) then
    		player.vy = game.ts
    	end
    end
    
    if not love.keyboard.isDown("z") then
    	player.hJmp = false
	
    end

    if (player.vy < 1) and canJump and love.keyboard.isDown("z") and 
						not (player.hJmp) then
    	-- print("blop")
    	player.vy = -player._jSpd
    	player.hJmp = true
    end
    
    if love.keyboard.isDown("left") then
    	player.dx = player.dx - player._mSpd
    	if (chk_collwall() or collides(inviswall)) then
    		player.dx = player.dx + player._mSpd
    	end
    	player.dir = "left"
    end

    if love.keyboard.isDown("right") then
    	player.dx = player.dx + player._mSpd
    	if (chk_collwall() or collides(inviswall)) then
    		player.dx = player.dx - player._mSpd
    	end
    	player.dir = "right"
    end
    -- print(player.vy .. "	" .. prbool(canJump) .. " " .. prbool(player.hJmp))
    applyMov()
end

function collision(dt)
    if (chk_colllava()) then
    	player.x = 64.0
    	player.y = 32.0
    	player.vy = 0.1
    	player.dx = 0.0
    	player.dx = 0.0
    end

    if (collides(door)) then
    	level_ld(game.mapn)
    	game.mapn = game.mapn + 1
    	player.x = 64.0
    	player.y = 32.0
    	player.vy = 0.1
    	player.dx = 0.0
    	player.dx = 0.0
    end
end

function applyMov()
    player.x = player.x + player.dx
    player.y = player.y + player.dy

    if (player.dx > 0.2) then
    	player.dx = player.dx - world.aresx
    end
    if (player.dx < -0.2) then
    	player.dx = player.dx + world.aresx
    end
end

function collides(tilenum)
    xleft = math.ceil((player.x + player.dx + 2) / game.ts)
    xright = math.ceil((player.x + player.dx + player.w) / game.ts)
    ytop = math.ceil((player.y + player.dy ) / game.ts)
    ybottom = math.ceil((player.y + player.dy + player.h ) / game.ts)
    return (game.map[ytop][xleft] 	== tilenum) or
           (game.map[ytop][xright]	== tilenum) or
           (game.map[ybottom][xleft] 	== tilenum) or
           (game.map[ybottom][xright]	== tilenum) 
end

function tween_f()
    -- lava
    if (not tween.l.stpd_g) then
    	if (tween.l.g >= tween.l.min_g  and tween.l.g < tween.l.max_g) then
    		tween.l.g = tween.l.g + tween.l.stp_g
    	end
    	if (tween.l.g == tween.l.max_g) then
    		tween.l.stpd_g = true
    	end
    elseif (tween.l.stpd_g) then
    	if (tween.l.g > tween.l.min_g  and tween.l.g <= tween.l.max_g) then
    		tween.l.g = tween.l.g - tween.l.stp_g
    	end
    	if (tween.l.g == tween.l.min_g) then
    		tween.l.stpd_g = false
    	end
    end

    -- door
    -- A gold slide to the right revealing the grey when there is a collision.
end
 
function level_ld(l)
    -- print(l)
    if (l == INTRO) then
    	game.map = intro 
    elseif (l == TRUST_ME_ON_THIS) then
    	game.map = trust_me_on_this
    elseif (l == LEAP_OF_FAITH) then
    	game.map = leap_of_faith
    elseif (l == BUT_YOU_TRUST_ME) then
    	game.map = but_you_trust_me
    elseif (l == STILL_TRUST_ME) then
    	game.map = still_trust_me
    elseif (l == PIT_OF_DOOM) then
    	game.map = pit_of_doom
    elseif (l == ABRUPT_OCCULT_FALL) then
    -- 	game.map = abrupt_occult_fall
        game.mapn = FALL_TO_DEATH
        game.map = fall_to_death
    elseif (l == FALL_TO_DEATH) then
    	game.map = fall_to_death
    elseif (l == HAVE_FAITH_DEAR) then
    	game.map = have_faith_dear
    else
    	love.event.quit()
    end
end

function chk_collwall()
    return collides(wall[1]) or collides(inviswall[1]) or
	   collides(wall[2]) or collides(inviswall[2]) or
	   collides(wall[3]) or collides(inviswall[3]) or
	   collides(wall[4]) or collides(inviswall[4]) 
end

function chk_colllava()
    return collides(lava[1]) or collides(invislava[1]) or
	   collides(lava[2]) or collides(invislava[2]) or
	   collides(lava[3]) or collides(invislava[3]) or
	   collides(lava[4]) or collides(invislava[4])
end

function prbool(bool)
    if bool == true then
    	return "1"
    elseif bool == false then
    	return "0"
    end
end
